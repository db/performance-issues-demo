package cern.ch.java.performance;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Servlet implementation class WaitingOnDatabase
 */
@WebServlet("/ClassLoadIssue")
public class ClassLoadIssue extends HttpServlet {
	private static final long serialVersionUID = 1L;

	final static Logger logger = LogManager.getLogger(ClassLoadIssue.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		logger.debug("This is debug");
		logger.error("This is error...");
		logger.info("This is info...");
		logger.warn("This is warning...");
		logger.fatal("This is fatal...");
		logger.trace("This is trace...");
		System.out.println("Console output");
		out.println("LOG4J issues...");
	}
}
