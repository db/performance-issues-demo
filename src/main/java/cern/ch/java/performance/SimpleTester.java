package lurodrig.performance.utils;


public class SimpleTester {
	
	// http://stackoverflow.com/questions/52353/in-java-what-is-the-best-way-to-determine-the-size-of-an-object

	public static void main(String[] args) throws InterruptedException {
		long maxMemoryInBytes = Runtime.getRuntime().maxMemory();
		long totalMemoryInBytes = Runtime.getRuntime().totalMemory();
		long freeMemoryInBytes = Runtime.getRuntime().freeMemory();
		long usedMemoryInBytes = totalMemoryInBytes - freeMemoryInBytes;
		long totalFreeMemoryInBytes = Runtime.getRuntime().maxMemory()
				- usedMemoryInBytes;
		int mb = 1024 * 1024;
		System.out
				.println("Total designated memory, this will equal the configured -Xmx value: "
						+ maxMemoryInBytes
						/ mb
						+ " Mbytes ("
						+ maxMemoryInBytes + " bytes )");
		System.out
				.println("Total allocated memory, is the total allocated space reserved for the java process: "
						+ totalMemoryInBytes
						/ mb
						+ " Mbytes ("
						+ totalMemoryInBytes + " bytes )");
		System.out
				.println("Current allocated free memory, is the current allocated space ready for new objects. Caution this is not the total free available memory: "
						+ freeMemoryInBytes
						/ mb
						+ " Mbytes ("
						+ freeMemoryInBytes + " bytes )");
		System.out
				.println("Used memory (currently used by instantiated objects) "
						+ usedMemoryInBytes
						/ mb
						+ " Mbytes ("
						+ usedMemoryInBytes + " bytes )");
		System.out
				.println("Total free memory (Runtime.getRuntime().maxMemory() - usedMemory) "
						+ totalFreeMemoryInBytes
						/ mb
						+ " Mbytes ("
						+ totalFreeMemoryInBytes + " bytes )");
		int maxNumberOfIterations = (int) (totalFreeMemoryInBytes / mb);
		String[] strings = new String[maxNumberOfIterations];
		int i = 0;
		System.out.println();
		System.out.println("The maximum number of iterations should be "
				+ maxNumberOfIterations);
		System.out.println();
		System.out.println();
		while (i < maxNumberOfIterations) {
			System.out.println("ITERATION " + i);
			strings[i] = new String(new char[mb]);
			totalMemoryInBytes = Runtime.getRuntime().totalMemory();
			freeMemoryInBytes = Runtime.getRuntime().freeMemory();
			usedMemoryInBytes = totalMemoryInBytes - freeMemoryInBytes;
			totalFreeMemoryInBytes = Runtime.getRuntime().maxMemory()
					- usedMemoryInBytes;	
			System.out
					.println("Used memory (currently used by instantiated objects): "
							+ usedMemoryInBytes
							/ mb
							+ " Mbytes ("
							+ usedMemoryInBytes + " bytes)");
			System.out
					.println("Total free memory: "
							+ totalFreeMemoryInBytes
							/ mb
							+ " Mbytes ("
							+ totalFreeMemoryInBytes + " bytes)");
			System.out.println(" ");
			i++;
		}
	}
}
