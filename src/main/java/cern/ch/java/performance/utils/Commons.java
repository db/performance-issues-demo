package cern.ch.java.performance.utils;

import cern.ch.java.performance.WaitingOnIO;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javassist.ClassPool;

import javax.servlet.http.HttpServletRequest;

import de.svenjacobs.loremipsum.LoremIpsum;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Commons {

    public static Connection getConnection(String username, String password,
            String driver, String url) throws ClassNotFoundException,
            SQLException {
        Class.forName(driver);
        Connection conn = DriverManager.getConnection(url, username, password);
        return conn;
    }

    public static void sleep(String sleeper, long millis) {
        try {
            Logger.getLogger(Commons.class.getName()).log(Level.FINE, null, sleeper + " is having a little nap of " + millis + " milliseconds");
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            Logger.getLogger(Commons.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public static int getInt(HttpServletRequest request, String name, int defaultValue) {
        int i;
        try {
            i = Integer.valueOf(request.getParameter(name));
        } catch (NumberFormatException e) {
            i = defaultValue;
            Logger.getLogger(Commons.class.getName()).log(Level.FINE, null, "Request from "
                    + request.getHeader(Constants.USER_AGENT)
                    + " has no int parameter, set default to " + defaultValue);
        }
        return i;
    }

    public static Properties getProperties(String filePath) throws IOException {
        Properties properties = new Properties();
        Reader inStream = new BufferedReader(new FileReader(filePath));
        properties.load(inStream);
        return properties;
    }

    public static void print(PrintWriter out, Iterator it) {
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            out.println(pair.getKey() + " = " + pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException
        }
    }

    public static void printUsage(PrintWriter out) {
        OperatingSystemMXBean operatingSystemMXBean = ManagementFactory
                .getOperatingSystemMXBean();
        out.print("\n");
        for (Method method : operatingSystemMXBean.getClass()
                .getDeclaredMethods()) {
            method.setAccessible(true);
            if (method.getName().startsWith("get")
                    && Modifier.isPublic(method.getModifiers())) {
                Object value;
                try {
                    value = method.invoke(operatingSystemMXBean);
                } catch (Exception e) {
                    value = e;
                    e.printStackTrace();
                }
                out.println("\t" + method.getName() + " = " + value);
            }
        }
    }

    public static Class generate(String name) throws Exception {
        ClassPool pool = ClassPool.getDefault();
        return pool.makeClass(name).toClass();
    }

    public static void writeAndSleep(String filePath, String sleeper,
            int seconds) throws IOException {
        LoremIpsum loremIpsum = new LoremIpsum();
        File file = new File(filePath);
        BufferedWriter output = new BufferedWriter(new FileWriter(file));
        for (int i = 0; i < 1000; i++) {
            String words = loremIpsum.getWords(100);
            output.write(words);
            System.out.println(sleeper + " writing " + words.substring(0, 5)
                    + "...");
            Commons.sleep(WaitingOnIO.class.getName(), seconds);
        }
        output.close();
    }
}
