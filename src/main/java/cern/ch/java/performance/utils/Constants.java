package cern.ch.java.performance.utils;

public class Constants {
	public static final String DB_CONNECTION_USER = "XXXXX";
	public static final String DB_CONNECTION_PASSWORD = "XXXXX";
	public static final String DB_CONNECTION_DRIVER = "oracle.jdbc.OracleDriver";
	public static final String DB_CONNECTION_URL = "jdbc:oracle:thin:@devdb11";
	public static final String USER_AGENT = "User-Agent";
	public static final String TMP_DIR = "java.io.tmpdir";
	public static final String IO_FILE = "/waitingForIO.txt";
	public static final String DB_PROCEDURE = "{call lurodrig.waiting_in_db(?)}";
	public static final String USE_DEFAULT_PROPERTIES = "useDefaultProperties";
	public static final String DEFAULT_PROPERTIES = "default.properties";
}
