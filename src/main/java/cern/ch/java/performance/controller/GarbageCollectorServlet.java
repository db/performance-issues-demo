/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.java.performance.controller;

import cern.ch.java.performance.commands.Command;
import cern.ch.java.performance.commands.TimerExecutorThread;
import cern.ch.java.performance.commands.gc.GarbageCollectorCommand;
import cern.ch.java.performance.utils.Commons;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author lurodrig
 */
@WebServlet(name = "GarbageCollectorServlet", urlPatterns = {"/GarbageCollectorServlet"})
public class GarbageCollectorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet GarbageCollectorServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet GarbageCollectorServlet at " + request.getContextPath() + "</h1>");
            int duration = Commons.getInt(request, "duration", 10);
            int iterations = Commons.getInt(request, "iterations", 10);
            out.println("<p>Servlet GarbageCollectorServlet should trigger lof of garbage collection during during " + duration + " seconds...</p>");
            Command command = new GarbageCollectorCommand(iterations);
            TimerExecutorThread executorThread = new TimerExecutorThread(command, duration, 0);
            executorThread.start();
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
