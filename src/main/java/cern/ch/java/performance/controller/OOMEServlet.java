/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.java.performance.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author lurodrig
 */
@WebServlet(name = "OOMEServlet", urlPatterns = {"/OOMEServlet"})
public class OOMEServlet extends HttpServlet {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OOMEServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OOMEServlet at " + request.getContextPath() + "</h1>");
            int dummyArraySize = 15;
            logger.info("Max JVM memory: " + Runtime.getRuntime().maxMemory());
            long memoryConsumed = 0;
            try {
                long[] memoryAllocated = null;
                for (int loop = 0; loop < Integer.MAX_VALUE; loop++) {
                    memoryAllocated = new long[dummyArraySize];
                    memoryAllocated[0] = 0;
                    memoryConsumed += dummyArraySize * Long.SIZE;
                    logger.info("Memory Consumed till now: " + memoryConsumed);
                    dummyArraySize *= dummyArraySize * 2;
                    Thread.sleep(500);
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(OOMEServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
