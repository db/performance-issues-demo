package cern.ch.java.performance;

import cern.ch.java.performance.utils.Commons;
import cern.ch.java.performance.utils.Constants;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class WaitingOnDatabase
 */
@WebServlet("/WaitingOnDatabaseOld")
public class WaitingOnDatabase extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        Connection connection = null;
        CallableStatement callableStatement = null;
        try {
            connection = Commons.getConnection(
                    Constants.DB_CONNECTION_USER,
                    Constants.DB_CONNECTION_PASSWORD,
                    Constants.DB_CONNECTION_DRIVER,
                    Constants.DB_CONNECTION_URL);
            String sql = Constants.DB_PROCEDURE;
            callableStatement = connection.prepareCall(sql);
            int seconds = Commons.getInt(request,"duration", 10);
            callableStatement.setInt(1, seconds);
            Date start = new Date();
            callableStatement.executeUpdate();
            Date finish = new Date();
            out.println("I have been in the database for so long...(from "
                    + start + " to " + finish + ")");
        } catch (ClassNotFoundException | SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // TODO Auto-generated catch block
         finally {
            try {
                callableStatement.close();
            } catch (Exception e) {
                e.printStackTrace(out);
            }
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace(out);
            }
        }
    }

}
