package cern.ch.java.performance;

import cern.ch.java.performance.utils.Commons;
import cern.ch.java.performance.utils.Constants;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SimpleTest {

	public static void main(String[] args) {
//		List<String> strings = new ArrayList<String>();
//		long maxMemoryInBytes = Runtime.getRuntime().maxMemory();
//		long totalMemoryInBytes = Runtime.getRuntime().totalMemory();
//		long freeMemoryInBytes = Runtime.getRuntime().freeMemory();
//		long usedMemoryInBytes = totalMemoryInBytes - freeMemoryInBytes;
//		long totalFreeMemoryInBytes = Runtime.getRuntime().maxMemory()
//				- usedMemoryInBytes;
//		int mb = 1024 * 1024;
//		System.out
//				.println("Total designated memory, this will equal the configured -Xmx value: "
//						+ maxMemoryInBytes
//						/ mb
//						+ " Mbytes ("
//						+ maxMemoryInBytes + " bytes )");
//		System.out
//				.println("Total allocated memory, is the total allocated space reserved for the java process: "
//						+ totalMemoryInBytes
//						/ mb
//						+ " Mbytes ("
//						+ totalMemoryInBytes + " bytes )");
//		System.out
//				.println("Current allocated free memory, is the current allocated space ready for new objects. Caution this is not the total free available memory: "
//						+ freeMemoryInBytes
//						/ mb
//						+ " Mbytes ("
//						+ freeMemoryInBytes + " bytes )");
//		System.out
//				.println("Used memory (currently used by instantiated objects) "
//						+ usedMemoryInBytes
//						/ mb
//						+ " Mbytes ("
//						+ usedMemoryInBytes + " bytes )");
//		System.out
//				.println("Total free memory (Runtime.getRuntime().maxMemory() - usedMemory)"
//						+ totalFreeMemoryInBytes
//						/ mb
//						+ " Mbytes ("
//						+ totalFreeMemoryInBytes + " bytes )");

//		long maxNumberOfIterations = maxMemoryInBytes / mb;

        Connection connection = null;

            CallableStatement callableStatement = null;
        try {
            connection = Commons.getConnection(
                    Constants.DB_CONNECTION_USER,
                    Constants.DB_CONNECTION_PASSWORD,
                    Constants.DB_CONNECTION_DRIVER,
                    Constants.DB_CONNECTION_URL);
            String sql = Constants.DB_PROCEDURE;
            callableStatement = connection.prepareCall(sql);
            int seconds = 10;
            callableStatement.setInt(1, seconds);
            Date start = new Date();
            callableStatement.executeUpdate();
            Date finish = new Date();
            System.out.println("I have been in the database for so long...(from "
                    + start + " to " + finish + ")");
        } catch (ClassNotFoundException | SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // TODO Auto-generated catch block
         finally {
            try {
                callableStatement.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

	}

}
