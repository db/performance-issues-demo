/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.java.performance;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lurodrig
 */
public class StuckThread extends Thread {
    
    private long duration;

    public StuckThread(long duration) {
        this.duration = duration;
    }
    
    @Override
    public void run() {
        try {
            Thread.sleep(duration);
        } catch (InterruptedException ex) {
            Logger.getLogger(StuckThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
