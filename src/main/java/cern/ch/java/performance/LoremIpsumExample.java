package lurodrig.perfomance.examples;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.svenjacobs.loremipsum.LoremIpsum;

/**
 * Servlet implementation class LoremIpsumExample
 */
@WebServlet(urlPatterns = { "/LoremIpsumExample" }, initParams = { @WebInitParam(name = "length", value = "128", description = "Default length of the lorem ipsum paragraph") })
//@ServletSecurity(@HttpConstraint(rolesAllowed = {"FederatedUsers"}))
public class LoremIpsumExample extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoremIpsumExample() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		LoremIpsum loremIpsum = new LoremIpsum();
		int length = 0;
		try {
			if (request.getParameter("length") != null) {
				length = Integer.valueOf(request.getParameter("length"));
			} else {
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			length = Integer.valueOf(getServletConfig().getInitParameter(
					"length"));
		}
		String msg = loremIpsum.getWords(length);
		out.print(msg);
	}

}
