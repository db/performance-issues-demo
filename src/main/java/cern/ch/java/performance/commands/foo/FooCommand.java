/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.java.performance.commands.foo;

import cern.ch.java.performance.commands.Command;
import java.io.IOException;
import java.nio.file.StandardOpenOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lurodrig
 */
public class FooCommand implements Command {

    @Override
    public void execute() {
        Path path = Paths.get("/tmp/test.out");
        try {
            Files.write(path, FooCommand.class.getCanonicalName().getBytes(), StandardOpenOption.APPEND, StandardOpenOption.CREATE);
        } catch (IOException ex) {
            Logger.getLogger(FooCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
