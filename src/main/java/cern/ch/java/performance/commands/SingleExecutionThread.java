/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.java.performance.commands;

import java.time.LocalTime;
import java.util.logging.Logger;

/**
 *
 * @author lurodrig
 */
public class SingleExecutionThread extends Thread {
    
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    
    private Command command;

    public SingleExecutionThread(Command command) {
        logger.info(command.getClass().getSimpleName() + " execution started at " + LocalTime.now());
        this.command = command;
        command.execute();
        logger.info(command.getClass().getSimpleName() + " execution finished at " + LocalTime.now());
    }

    @Override
    public void run() {
        command.execute();
    }
}
