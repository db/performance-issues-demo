/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.java.performance.commands.cpu;

import cern.ch.java.performance.commands.Command;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lurodrig
 */
public class BusyCpuCommand implements Command {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    private double load;
    private int threadNumber;

    public BusyCpuCommand(double load, int threadNumber) {
        this.load = load;
        this.threadNumber = threadNumber;
    }

    @Override
    public void execute() {
        if (System.currentTimeMillis() % 100 == 0) {
            long sleepTime = (long) Math.floor((1 - load) * 100);
            logger.info("Thread "+ this.threadNumber + " is going to sleep for " + sleepTime + " milliseconds");
            try {
                Thread.sleep((long) Math.floor((1 - load) * 100));
                logger.info("Thread " + this.threadNumber + ", wake up!!!");
            } catch (InterruptedException ex) {
                Logger.getLogger(BusyCpuCommand.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
