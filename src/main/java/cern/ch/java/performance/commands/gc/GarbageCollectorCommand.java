/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.java.performance.commands.gc;

import cern.ch.java.performance.commands.Command;

/**
 *
 * @author lurodrig
 */
public class GarbageCollectorCommand implements Command {

    private int iterations;

    public GarbageCollectorCommand(int iterations) {
        this.iterations = iterations;
    }
   
    @Override
    public void execute() {
        Color color = new Color("white");
        for (int i = 0; i < this.iterations; i++) {
            if (i % 2 == 1) {
                color = new Color("red");
            } else {
                color = new Color("green");
            }
            color = null;
        }
    }
}
