/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.java.performance.commands;

import cern.ch.java.performance.utils.Commons;
import java.time.LocalTime;
import java.util.logging.Logger;

/**
 *
 * @author lurodrig
 */
public class TimerExecutorThread extends Thread {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    private Command command;
    private long durationInMillis;
    private long pause;

    public TimerExecutorThread(Command command, int duration, long pause) {
        logger.info("Duration of the " + command.getClass().getSimpleName() + " action: " + duration + " seconds");
        logger.info("Pauses of " + pause + " milliseconds between executions");
        this.command = command;
        this.durationInMillis = duration * 1000;
        this.pause = pause;
    }

    @Override
    public void run() {
        long startTimeInMillis = System.currentTimeMillis();
        logger.info(command.getClass().getSimpleName() + " execution started at " + LocalTime.now());
        long currentTimeInMilis = System.currentTimeMillis() - startTimeInMillis;
        while (currentTimeInMilis < this.durationInMillis) {
            command.execute();
            Commons.sleep(command.getClass().getSimpleName(), this.pause);
            currentTimeInMilis = System.currentTimeMillis() - startTimeInMillis;
        }
        logger.info(command.getClass().getSimpleName() + " execution finished at " + LocalTime.now());
    }

}
