/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.java.performance.commands;

/**
 *
 * @author lurodrig
 */
public interface Command {
    public void execute();
}
