/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.java.performance.commands.gc;

import java.util.logging.Logger;

/**
 *
 * @author lurodrig
 */
public class Color {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    private String color;

    public Color(String color) {
        this.color = color;
    }

    @Override
    protected void finalize() throws Throwable {
        logger.finest(this.color + " cleaned");
    }

}
