/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.java.performance.commands.heap;

/**
 *
 * @author lurodrig
 */
public class Key {
    
    private int capacity;
    private Integer id;
    private StringBuffer buffer;

    public Key(Integer id, int capacity) {
        this.id = id;
        this.capacity = capacity;
        buffer = new StringBuffer(this.capacity*1024);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
