/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.java.performance.commands.heap;

import cern.ch.java.performance.commands.Command;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 * @author lurodrig
 */
public class KeyLessEntryCommand implements Command {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    int capacity;
    
    public KeyLessEntryCommand(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public void execute() {
        Map m = new HashMap();
        for (int i = 0; i < 10000; i++) {
            if (!m.containsKey(new Key(i, this.capacity))) {
                m.put(new Key(i, this.capacity), "Number:" + i);
            }
            long free = Runtime.getRuntime().freeMemory();
            logger.fine(free/(1024 * 1024) + " MB free");
            logger.fine((Runtime.getRuntime().totalMemory() - free)/(1024 * 1024) + " MB used");
        }
    }
}
