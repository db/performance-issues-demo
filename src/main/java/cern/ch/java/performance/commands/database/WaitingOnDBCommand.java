/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.java.performance.commands.database;

import cern.ch.java.performance.commands.Command;
import cern.ch.java.performance.utils.Commons;
import cern.ch.java.performance.utils.Constants;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lurodrig
 */
public class WaitingOnDBCommand implements Command {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    private int duration;

    public WaitingOnDBCommand(int duration) {
        this.duration = duration;
    }

    @Override
    public void execute() {
        Connection connection = null;
        CallableStatement callableStatement = null;
        try {
            connection = Commons.getConnection(
                    Constants.DB_CONNECTION_USER,
                    Constants.DB_CONNECTION_PASSWORD,
                    Constants.DB_CONNECTION_DRIVER,
                    Constants.DB_CONNECTION_URL);
            callableStatement = connection.prepareCall(Constants.DB_PROCEDURE);
            callableStatement.setInt(1, duration);
            callableStatement.execute();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(WaitingOnDBCommand.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                callableStatement.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, null, e);
            }
            try {
                connection.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, null, e);
            }
        }

    }

}
