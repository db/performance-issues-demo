package cern.ch.java.performance;

import cern.ch.java.performance.utils.Commons;
import cern.ch.java.performance.utils.Constants;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class WaitingOnDatabase
 */
@WebServlet("/SimpleDBExample")
public class SimpleDBExample extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			// Get connection
			connection = Commons.getConnection(Constants.DB_CONNECTION_USER,
					Constants.DB_CONNECTION_PASSWORD,
					Constants.DB_CONNECTION_DRIVER,
					Constants.DB_CONNECTION_URL);
			String query = "SELECT * FROM " + request.getParameter("table");
			statement = connection.prepareStatement(query);
			ResultSetMetaData resultSetMetaData = statement.getMetaData();
			printColTypes(resultSetMetaData, out);
			int numberOfColumns = resultSetMetaData.getColumnCount();
			for (int i = 1; i <= numberOfColumns; i++) {
				if (i > 1)
					System.out.print(",  ");
				String columnName = resultSetMetaData.getColumnName(i);
				out.print(columnName);
			}
			System.out.println("");
			ResultSet resultSet = statement.executeQuery();
			if (resultSet != null) {
				while (resultSet.next()) {
					for (int i = 1; i <= numberOfColumns; i++) {
						if (i > 1)
							out.print(",  ");
						String columnValue = resultSet.getString(i);
						out.print(columnValue);
					}
					out.println("");
				}
			}
		} catch (SQLException ex) {
			ex.printStackTrace(out);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
			} catch (Exception e) {
				e.printStackTrace(out);
			}
			try {
				connection.close();
			} catch (Exception e) {
				e.printStackTrace(out);
			}
		}
	}

	private void printColTypes(ResultSetMetaData rsmd, PrintWriter out)
			throws SQLException {
		int columns = rsmd.getColumnCount();
		for (int i = 1; i <= columns; i++) {
			int jdbcType = rsmd.getColumnType(i);
			String name = rsmd.getColumnTypeName(i);
			out.print("Column " + i + " is JDBC type " + jdbcType);
			out.println(", which the DBMS calls " + name);
		}
	}

}
