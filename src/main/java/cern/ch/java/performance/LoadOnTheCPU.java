/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.java.performance;

/**
 *
 * @author lurodrig
 */
public class LoadOnTheCPU {

    /**
     * Starts the Load Generation
     *
     * @param args Command line arguments, ignored
     */
    public static void main(String[] args) {
        int numCore = Runtime.getRuntime().availableProcessors();
        System.out.println("Cores: " + numCore);
        int numThreadsPerCore = 2;
        double load = 0.8;
        final long duration = 100000;
        for (int thread = 0; thread < numCore * numThreadsPerCore; thread++) {
            new BusyThread("Thread" + thread, load, duration).start();
        }
    }

    /**
     * Thread that actually generates the given load
     *
     * @author Sriram
     */
    public static class BusyThread extends Thread {

        private double load;
        private long duration;

        /**
         * Constructor which creates the thread
         *
         * @param name Name of this thread
         * @param load Load % that this thread should generate
         * @param duration Duration that this thread should generate the load
         * for
         */
        public BusyThread(String name, double load, long duration) {
            super(name);
            this.load = load;
            this.duration = duration;
        }

        /**
         * Generates the load when run
         */
        @Override
        public void run() {
            long startTime = System.currentTimeMillis();
            try {
                // Loop for the given duration
                long currentTimeInMilis = System.currentTimeMillis() - startTime;
                System.out.println("Duration: " + duration);
                while (currentTimeInMilis < duration) {
                    // Every 100ms, sleep for the percentage of unladen time
                    if (System.currentTimeMillis() % 100 == 0) {
                        Thread.sleep((long) Math.floor((1 - load) * 100));
                    }
                    currentTimeInMilis = System.currentTimeMillis() - startTime;
                    System.out.println(currentTimeInMilis);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
