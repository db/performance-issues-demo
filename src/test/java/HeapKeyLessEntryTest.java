/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import cern.ch.java.performance.commands.TimerExecutorThread;
import cern.ch.java.performance.commands.heap.KeyLessEntryCommand;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author lurodrig
 */
public class HeapKeyLessEntryTest extends PerformanceTest {

    public HeapKeyLessEntryTest() {
        int keySize = 2;
        command = new KeyLessEntryCommand(keySize);
        executorThread = new TimerExecutorThread(command, durationInSeconds, pauseInMillis);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void execute_test_a() throws InterruptedException {
        super.execute();
    }
}
