
import cern.ch.java.performance.commands.Command;
import cern.ch.java.performance.commands.TimerExecutorThread;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author lurodrig
 */
class PerformanceTest {

    protected Command command;
    protected TimerExecutorThread executorThread;
    protected int durationInSeconds = 10;
    protected long pauseInMillis = 0;
    protected long waitInMillis = 1000;
    
    protected void execute() throws InterruptedException{
        executorThread.start();
        while (executorThread.isAlive()) {
            executorThread.join(waitInMillis);
        }
    }
}
